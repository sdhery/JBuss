package com.yuzi.handler;

import com.jfinal.aop.Clear;
import com.jfinal.handler.Handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@Clear
public class HtmSkipHandler extends Handler {
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        int index = target.lastIndexOf(".html");
        if (index != -1)
            target = target.substring(0, index);
        next.handle(target, request, response, isHandled);
    }
}
