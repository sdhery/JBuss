package com.yuzi.service.systemSer;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.model.systemMo.SysButton;
import com.yuzi.model.systemMo.SysRoleMenuButton;
import com.yuzi.service.BaseSer.BaseService;

import java.sql.SQLException;
import java.util.List;

public class SysButtonService extends BaseService {

    public static final SysButtonService dao = new SysButtonService();

    private SysButton sysButtonDao;

    public List<SysButton> findAllButtonByMenuId(Integer menuId){
        return sysButtonDao.findAllButtonByMenuId(menuId);
    }

    public boolean addButton(SysButton sysButton){
        String code = generateBtnCode(sysButton);
        sysButton.set("code",code);
        SysRoleMenuButton sysRoleMenuButton = new SysRoleMenuButton();
        sysRoleMenuButton.set("role_id",1);
        sysRoleMenuButton.set("menu_id",sysButton.getInt("menu_id"));
        sysRoleMenuButton.set("button_id",sysButton.getInt("id"));
        boolean res = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                boolean isOk1 = sysButton.addSysButton(sysButton);
                boolean isOk2 = SysRoleMenuButtonService.dao.addSysRoleMenuButton(sysRoleMenuButton);
                if (isOk1 && isOk2){
                    return true;
                }else {
                    return false;
                }
            }
        });
        return res;
    }

    //生成按钮code规则
    public String generateBtnCode(SysButton sysBut){
        int btnId = sysButtonDao.getMaxIndex() + 1;
        sysBut.set("id",btnId);
        int menuId = sysBut.getInt("menu_id");
        return "M" + menuId + "B" + btnId + "_" + sysBut.getStr("code");
    }
    public boolean delButton(SysRoleMenuButton sysRoleMenuButton){
        boolean res = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                boolean isOk1 = SysRoleMenuButtonService.dao.delSysRoleMenuButton(sysRoleMenuButton);
                boolean isOk2 = sysButtonDao.delSysButton(sysRoleMenuButton.getInt("button_id"));
                if (isOk1 && isOk2){
                    return true;
                }else {
                    return false;
                }
            }
        });
        return res;
    }

    public SysButton findButtonByButtonId(Integer buttonId){ return sysButtonDao.findButtonByButtonId(buttonId); }

    public boolean editButton(SysButton sysbutton){ return sysbutton.update(); }

    public boolean updateSysButtonByisHidden(Integer buttonId,String isHidden){return sysButtonDao.updateSysButtonByisHidden(buttonId,isHidden); }

    public boolean findSysBtnByBtnCodeAndRoleId(String btnCode,Integer roleId){
        return sysButtonDao.findSysBtnByBtnCodeAndRoleId(btnCode,roleId);
    }

    public List<Record> findSysRoleMenuBtnByRoleIdAndMenuId(Integer roleId,Integer menuId){
        return sysButtonDao.findSysRoleMenuBtnByRoleIdAndMenuId(roleId,menuId);
    }
}
