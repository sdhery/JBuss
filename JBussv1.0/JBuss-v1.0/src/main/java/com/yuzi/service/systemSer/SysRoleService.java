package com.yuzi.service.systemSer;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.model.systemMo.SysRole;
import com.yuzi.model.systemMo.SysRoleMenu;
import com.yuzi.model.systemMo.SysRoleMenuButton;
import com.yuzi.model.systemMo.SysUserRole;
import com.yuzi.service.BaseSer.BaseService;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SysRoleService extends BaseService {
	private SysRole sysRoleDao;

	private SysUserRole sysUserRoleDao;

	private SysRoleMenu sysRoleMenuDao;

	private SysRoleMenuButton sysRoleMenuButtonDao;

	public Page<Record> findAllSysRole(int pageNumber, int pageSize, String roleName) {
		return sysRoleDao.findAllSysRole(pageNumber, pageSize, roleName);
	}

	public Map<String, String> deleteSysRole(int id) {
		final Map<String, String> map = new HashMap<>();

		if (id == Constant.SysConfig.sys_role_id) {
			map.put("flag", "false");
			map.put("msg", "禁止删除该角色");
			return map;
		}

		boolean res = Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {

				boolean isDelete = sysRoleDao.deleteById(id);
				if (isDelete) {
					// 将权限表中的关联关系删掉（sys_user_role,sys_role_menu）,通过role_id
					sysUserRoleDao.deleteSysUserRoleByRoleId(id);
					sysRoleMenuDao.deleteSysRoleMenuByRoleId(id);
					sysRoleMenuDao.deleteSysRoleMenuByBothId(id, 0);
					sysRoleMenuButtonDao.delSysRoleMenuButtonByRoleId(id);
					map.put("flag", "true");
					map.put("msg", "删除成功");
					return true;
				} else {
					map.put("flag", "false");
					map.put("msg", "删除失败");
					return false;
				}
			}
		});
		return map;
	}

	public Map<String, String> addSysRole(SysRole sysRole) {

		final Map<String, String> map = new HashMap<>();

		boolean res = Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				int roleId = sysRoleDao.addSysRole(sysRole);
				if (roleId > 0) {
					// 添加角色的同时，需要添加Root菜单
					boolean isOk = sysRoleMenuDao.addSysRoleMenu(roleId, 0);
					if (isOk) {
						map.put("flag", "true");
						map.put("msg", "添加成功");
						return true;
					} else {
						map.put("flag", "false");
						map.put("msg", "添加失败");
						return false;
					}
				} else {
					map.put("flag", "false");
					map.put("msg", "添加失败");
					return false;
				}
			}
		});
		return map;
	}

	public SysRole getSysRoleById(int id) {
		return sysRoleDao.findById(id);
	}

	public boolean updateSysRole(SysRole sysRole) {
		return sysRoleDao.updateSysRole(sysRole);
	}

	public boolean updateSysRoleStatusById(int roleId, String status) {
		SysRole sysRole = getSysRoleById(roleId);
		sysRole.set("status", status);
		return sysRoleDao.updateSysRole(sysRole);
	}

	public List<SysRole> findSysRoleByCondition(String status) {
		return sysRoleDao.findSysRoleByCondition(status);
	}

	public SysRole findSysRoleByUserId(Integer userId){
		return sysRoleDao.findSysRoleByUserId(userId);
	}
}
