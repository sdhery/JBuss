package com.yuzi.utils;

import com.jfinal.kit.PropKit;
import com.jfinal.upload.UploadFile;

import java.io.File;

public class UploadFileUtils {

    //final static String uploadPath = PropKit.use("jConfig.properties").get("local.uploadPath");
    final static String uploadPath = PropKit.use("jConfig.properties").get("baseUploadPath");

    public static boolean uploadFileByReame(String oldName,String newName){
        File oldFile = new File(uploadPath + oldName);  //true
        if (!oldFile.exists()){
            return false;
        }
        File newFile = new File(uploadPath + newName);  //false
        return oldFile.renameTo(newFile);                       //这里不是复制，而是移动，所以老的文件会被删除
    }

    public static String uploadCommonFile(UploadFile uploadFile){
        File file = uploadFile.getFile();
        return file.getName();
        /*String filePath = file.getPath();
        String fileName = filePath.substring(filePath.lastIndexOf(File.separator)+1,filePath.length());
        String ftpPath = Constant.FileType.filePath(Constant.FileType.customer,Constant.FileType.staff);
        String suffix = "";
        if (fileName.lastIndexOf(".") != -1){
            suffix = fileName.substring(fileName.lastIndexOf("."),fileName.length());
        }
        String savePath = DataUtils.getUUID() + suffix;
        return FTPUtils.getInstance().uploadLocalFile(ftpPath,filePath,savePath);*/
    }

    public static boolean deleteFile(String fileName){
        String filepath = uploadPath + fileName;
        File file = new File(filepath);
        if (!file.exists()){
            return true;
        }
        return file.delete();
    }
}
