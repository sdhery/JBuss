package com.yuzi.model.systemMo;

import com.jfinal.plugin.activerecord.Db;
import com.yuzi.model.BaseMo.BaseModel;

public class SysRoleMenuButton extends BaseModel<SysRoleMenuButton> {

    public boolean delSysRoleMenuButton(SysRoleMenuButton sysRoleMenuButton){
        String sql="delete from sys_role_menu_button where button_id=? and role_id=? and menu_id=? ";
        return Db.delete(sql,sysRoleMenuButton.get("button_id"),sysRoleMenuButton.get("role_id"),sysRoleMenuButton.get("menu_id"))>0 ? true:false;
    }

    public boolean delSysRoleMenuButtonByRoleId(int roleId){
        String sql = "delete from sys_role_menu_button where role_id=? ";
        return Db.delete(sql,roleId)>0 ? true:false;
    }

}
