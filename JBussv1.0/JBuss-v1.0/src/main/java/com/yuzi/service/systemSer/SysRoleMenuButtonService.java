package com.yuzi.service.systemSer;

import com.yuzi.model.systemMo.SysRoleMenuButton;
import com.yuzi.service.BaseSer.BaseService;

import java.util.HashMap;
import java.util.Map;

public class SysRoleMenuButtonService extends BaseService {

    public static final SysRoleMenuButtonService dao = new SysRoleMenuButtonService();
    private SysRoleMenuButton sysRoleMenuButtonDao;

    public Map updateSysRoleMenuButton(SysRoleMenuButton sysRoleMenuButton, String action){
        Map<String,String> map=new HashMap<String, String>();
        boolean b=false;
        if("del".equals(action)) {
            b = sysRoleMenuButton.delSysRoleMenuButton(sysRoleMenuButton);
            if (b) {
                map.put("flag", "true");
                map.put("msg","取消授权成功");
            } else {
                map.put("flag", "false");
                map.put("msg","取消授权失败");
            }
        } else if (action.equals("add")){
            b=addSysRoleMenuButton(sysRoleMenuButton);
            if (b) {
                map.put("flag", "true");
                map.put("msg","授权成功");
            } else {
                map.put("flag", "false");
                map.put("msg","授权失败");
            }
        }
        return map;

    }

    public boolean addSysRoleMenuButton(SysRoleMenuButton sysRoleMenuButton){
        return sysRoleMenuButton.save();
    }

    public boolean delSysRoleMenuButton(SysRoleMenuButton sysRoleMenuButton){
        return sysRoleMenuButtonDao.delSysRoleMenuButton(sysRoleMenuButton);
    }
}
