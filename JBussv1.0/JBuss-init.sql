/*
SQLyog Ultimate
MySQL - 8.0.11 : Database - loanmarket_develop_sources
*********************************************************************
*/



USE `JBuss`;

/*Table structure for table `sys_button` */

CREATE TABLE `sys_button` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL COMMENT '编码生成规则：B + menu_id + button_id\r\n            例如：菜单ID：12；按钮ID：23，则按钮编码：B1223\r\n            ',
  `isHidden` char(1) DEFAULT '0',
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

/*Data for the table `sys_button` */

insert  into `sys_button`(`id`,`menu_id`,`name`,`code`,`isHidden`,`note`) values 
(5,24,'顶顶顶','M24B5','1',NULL),
(6,29,'添加','M29B6_add','0',NULL),
(7,29,'编辑','M29B7_edit','0',NULL),
(8,29,'删除','M29B8_del','0',NULL),
(9,29,'禁用','M29B9_dis','0',NULL),
(10,30,'添加','M30B10_add','0',NULL),
(11,30,'编辑','M30B11_edit','0',NULL),
(12,30,'删除','M30B12_del','0',NULL),
(13,31,'编辑','M31B13_edit','0',NULL),
(14,32,'添加','M32B14_add','0',NULL),
(15,32,'编辑','M32B15_edit','0',NULL),
(16,32,'删除','M32B16_del','0',NULL),
(17,32,'改密','M32B17_pwd','0',NULL),
(18,32,'禁用','M32B18_dis','0',NULL),
(19,33,'编辑_菜单','M33B19_edit','0',NULL),
(20,33,'编辑_按钮','M33B20_edit','0',NULL),
(21,36,'添加','M36B21_add','0',NULL),
(22,36,'删除','M36B22_del','0',NULL),
(23,36,'编辑','M36B23_edit','0',NULL),
(24,36,'禁用','M36B24_dis','0',NULL),
(49,40,'添加','M40B49_add','0',NULL),
(50,40,'删除','M40B50_del','0',NULL),
(51,40,'编辑','M40B51_edit','0',NULL),
(56,53,'新建渠道','M53B56_add','0',NULL),
(57,38,'添加分组','M38B57_add','0',NULL),
(58,38,'禁用','M38B58_dis','0',NULL),
(59,38,'编辑分组','M38B59_edit','0',NULL),
(60,38,'删除分组','M38B60_del','0',NULL),
(61,39,'添加产品','M39B61_add','0',NULL),
(62,39,'编辑产品','M39B62_edit','0',NULL),
(63,39,'禁用','M39B63_dis','0',NULL),
(64,39,'删除产品','M39B64_del','0',NULL),
(65,44,'添加产品','M44B65_add','0',NULL),
(66,44,'禁用','M44B66_dis','0',NULL),
(67,44,'编辑','M44B67_edit','0',NULL),
(68,44,'删除','M44B68_del','0',NULL),
(69,45,'添加','M45B69_add','0',NULL),
(70,45,'预览','M45B70_view','0',NULL),
(71,45,'禁用','M45B71_dis','0',NULL),
(72,45,'删除','M45B72_del','0',NULL),
(73,45,'编辑','M45B73_edit','0',NULL),
(74,44,'预览','M44B74_view','0',NULL),
(75,53,'修改渠道','M53B75_edit','0',NULL),
(76,49,'添加图片','M49B76_add','0',NULL),
(77,49,'预览幻灯','M49B77_view','0',NULL),
(78,49,'禁用','M49B78_dis','0',NULL),
(79,49,'修改幻灯','M49B79_edit','0',NULL),
(80,49,'删除幻灯','M49B80_del','0',NULL),
(81,50,'选择已有产品','M50B81_add','0',NULL),
(82,50,'修改排序','M50B82_edit','0',NULL),
(83,50,'删除关联','M50B83_del','0',NULL),
(84,47,'新建分组','M47B84_add','0',NULL),
(85,47,'禁用','M47B85_dis','0',NULL),
(86,47,'修改','M47B86_edit','0',NULL),
(87,47,'删除','M47B87_del','0',NULL);

/*Table structure for table `sys_dict` */

CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Pcode` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

/*Data for the table `sys_dict` */

insert  into `sys_dict`(`id`,`Pcode`,`name`,`code`,`note`) values 
(1,'0','角色状态','roleStatus',NULL),
(2,'roleStatus','启用','on',NULL),
(3,'roleStatus','禁用','off',NULL),
(7,'0','用户状态','userStatus',NULL),
(8,'userStatus','启用','on',NULL),
(9,'userStatus','禁用','off',NULL),
(10,'0','按钮','button','配置按钮，千万别乱动'),
(11,'button','预览','view','view'),
(12,'button','添加','add','add'),
(13,'button','编辑','edit','edit'),
(14,'button','删除','del','delete'),
(15,'button','禁用','dis','disable'),
(16,'button','下载','down','download'),
(17,'button','上传','up','upload'),
(18,'button','改密','pwd','password'),
(19,'button','踢除','kick','Kicking'),
(47,'0','是否','yesOrno',NULL),
(48,'yesOrno','是','yes',''),
(49,'yesOrno','否','no',''),
(61,'button','子菜单','subList','');

/*Table structure for table `sys_menu` */

CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Pid` int(11) DEFAULT '0',
  `code` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `isHidden` char(1) DEFAULT '0',
  `icon` varchar(128) DEFAULT NULL,
  `sort` varchar(5) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`id`,`Pid`,`code`,`name`,`url`,`isHidden`,`icon`,`sort`,`note`) values 
(0,-1,'root','根菜单',NULL,'0',NULL,NULL,NULL),
(28,0,'sysManage','系统管理',NULL,'0','','7',NULL),
(29,28,'role','角色管理','/admin/roleList.html','0','&#xe62d;',NULL,NULL),
(30,28,'cloumn','栏目管理','/admin/systemCategory.html','0','&#xe681;',NULL,NULL),
(31,28,'sys_dict','数据字典','/admin/dictList.html','0','',NULL,NULL),
(32,28,'userM','用户管理','/admin/userList.html','0','&#xe60d;','0','111'),
(33,28,'roleAuth','角色权限','/admin/roleAuth.html','0','&#xe70d;',NULL,NULL),
(36,28,'button','按钮配置','/admin/buttonList.html','0','&#xe63c;',NULL,NULL),
(59,0,'system Info','系统信息',NULL,'0','&#xe72d;','6',NULL),
(60,59,'druidMonitor','Druid Monitor','/druid/index.html','0','&#xe6b5;','0',NULL);

/*Table structure for table `sys_role` */

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `note` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`,`code`,`status`,`note`) values 
(1,'超级管理员','superadmin','on','ffff11'),
(3,'普通用户','user','on',NULL);

/*Table structure for table `sys_role_menu` */

CREATE TABLE `sys_role_menu` (
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  UNIQUE KEY `AK_Key_1` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`role_id`,`menu_id`) values 
(1,0),
(1,28),
(1,29),
(1,30),
(1,31),
(1,32),
(1,33),
(1,36),
(1,59),
(1,60),
(3,0);

/*Table structure for table `sys_role_menu_button` */

CREATE TABLE `sys_role_menu_button` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `button_id` int(11) NOT NULL,
  UNIQUE KEY `AK_Key_1` (`role_id`,`menu_id`,`button_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_menu_button` */

insert  into `sys_role_menu_button`(`role_id`,`menu_id`,`button_id`) values 
(1,38,34),
(1,38,35),
(1,38,36),
(1,38,37),
(1,38,38),
(1,38,57),
(1,38,58),
(1,38,59),
(1,38,60),
(1,39,39),
(1,39,40),
(1,39,41),
(1,39,42),
(1,39,61),
(1,39,62),
(1,39,63),
(1,39,64),
(1,44,65),
(1,44,66),
(1,44,67),
(1,44,68),
(1,44,74),
(1,45,69),
(1,45,70),
(1,45,71),
(1,45,72),
(1,45,73),
(1,47,84),
(1,47,85),
(1,47,86),
(1,47,87),
(1,49,76),
(1,49,77),
(1,49,78),
(1,49,79),
(1,49,80),
(1,50,81),
(1,50,82),
(1,50,83),
(1,53,75),
(3,38,34),
(3,38,40),
(3,38,41),
(3,38,47),
(3,39,43),
(3,39,44),
(3,39,46),
(3,39,55),
(3,39,61),
(3,39,62),
(3,39,63),
(3,39,64),
(3,40,49),
(3,40,50),
(3,40,51),
(3,44,65),
(3,44,66),
(3,44,67),
(3,44,68),
(3,45,69),
(3,45,70),
(3,45,71),
(3,45,72),
(3,45,73);

/*Table structure for table `sys_user` */

CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `plaintext` varchar(128) DEFAULT NULL,
  `createTime` varchar(64) DEFAULT NULL,
  `status` varchar(28) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`nick`,`username`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`nick`,`username`,`password`,`plaintext`,`createTime`,`status`,`note`) values 
(1,'超级管理员','superadmin','$shiro1$SHA-256$500000$a/K3p1Mfd5fqjIEyLhMPZQ==$coTCpp+Ks+wO4ouWnWS+Gukkl26U52oUGI5AWk0ugKk=','','2018-06-27 21:12:30','on','12356456'),
(3,'赵正义','zzyi','$shiro1$SHA-256$500000$ln4vfhuPlXXKL0kX9qqWyw==$G8O/fSTOLY94Q4QilCBJzTlE4n/J/i8Ywmv/U2nglAE=','123456','2019-04-10 17:29:16','on',NULL);

/*Table structure for table `sys_user_role` */

CREATE TABLE `sys_user_role` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  UNIQUE KEY `AK_Key_1` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`user_id`,`role_id`) values 
(1,1),
(3,3);

