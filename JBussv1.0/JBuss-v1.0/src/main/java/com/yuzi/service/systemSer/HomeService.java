package com.yuzi.service.systemSer;

import com.jfinal.plugin.activerecord.Model;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.UploadFileUtils;

public class HomeService extends BaseService {

    public boolean updateAnnex(Model model) {
        Object pdfpath=model.get("pdfpath");
        Object annexOne=model.get("annexOne");
        boolean isOk2 = UploadFileUtils.deleteFile(annexOne.toString());
        boolean isOk1 = true;
        if(pdfpath != null && pdfpath != ""){
            isOk1 = UploadFileUtils.deleteFile(pdfpath.toString());
        }
        if (isOk1 && isOk2) {
            model.set("pdfpath","");
            model.set("annexOne","");
            return model.update();
        } else {
            return false;
        }
    }



    public boolean updateFile(Model model,String column) {
        Object columnname=model.get(column);
        boolean isOk = UploadFileUtils.deleteFile(columnname.toString());
        if (isOk) {
            model.set(column,"");
            return model.update();
        } else {
            return false;
        }
    }
}
