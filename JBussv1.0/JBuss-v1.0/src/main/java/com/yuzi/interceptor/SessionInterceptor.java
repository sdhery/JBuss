package com.yuzi.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.yuzi.utils.SessionKit;
public class SessionInterceptor implements Interceptor {
    public void intercept(Invocation inv) {
        try {
            SessionKit.put(inv.getController().getSession(true));
            inv.invoke();
        }
        finally {
            SessionKit.remove();
        }
    }

}
